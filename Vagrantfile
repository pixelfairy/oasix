# -*- mode: ruby -*-
# vi: set ft=ruby :

prefix = "fd11::"
vars = {
  "prefix" => prefix,
  "router_address" => prefix+"10",
  "dns64_address" => prefix+"11",
  "legacy_prefix" => "fd11:0000:0000:ffff:ffff:ffff::/96",
  "tayga_v4_pool" => "192.168.255.0/24",
  "tayga_v4_address" => "192.168.255.1",
  "localzones" => {
    "ns.lan" => prefix+"13"
  }
}

client_script = <<SCRIPT
ip route delete default
ip route add default via #{vars["router_address"]} dev eth1
echo nameserver #{vars["router_address"]} > /etc/resolv.conf
SCRIPT

VAGRANTFILE_API_VERSION = "2"
Vagrant.configure(VAGRANTFILE_API_VERSION) do |config|
  config.hostmanager.enabled = true
  config.hostmanager.include_offline = true
  config.vm.box = "ubuntu/trusty64"

  config.vm.define "router" do |router|
    router.vm.hostname = "router"
    router.vm.network "private_network", ip: vars["router_address"]
    router.vm.provision "shell", inline: "ip address add #{vars["dns64_address"]} dev eth1"
    router.vm.provision "ansible" do |ansible|
      ansible.playbook = "vagrant/router/router.yml"
      ansible.extra_vars = vars
    end
  end

  config.vm.define "ns" do |ns|
    ns.vm.hostname = "ns.lan"
    ns.vm.network "private_network", ip: prefix+"13"
    ns.vm.provision "shell", inline: client_script
    ns.vm.provision "ansible", playbook: "vagrant/ns/ns.yml"
  end

  config.vm.define "client" do |client|
    client.vm.hostname = "client"
    client.vm.network "private_network", ip: prefix+"12"
    client.vm.provision "shell", inline: client_script
  end

end
