Oasix
=====

Ipv6 test network for vagrant when your stuck on an ipv4 only network. You can
use this as a starting point to make test harnesses.

All the vagrant related files, except Vagrantfile itself, are in their own
folder, out of the way of your own project.

It uses tayga for nat64 and bind for dns64.

This is needed for vagrant / virtualbox because the nat out of virtualbox is v4
only. There are workaround such as using a bridged interface, but ipv6 networks 
are not always available. This way, it doesn't matter.

DNS
---

the router runs unbound as the primary resolver. unbounds "upstream" is bind running
on a different address, var["dns64_address"], but on the same host.

bind is set to do dns64 for unbound, but return unmodified answers to anyone else.
this means, other clients can query var["dns64_address"] for the real, unmangled,
 address.

if you haven't changed any variables, var["dns64_address"] is fd11::11

As a convenience, you can add local zones to the routers unbound config. ns.lan
is a sample nameserver.

Ansible
-------

Ansible is a configuration management tool. Im using it because it makes it easy
to develop things like this. you dont have to know it to use this. if anyone
besides me uses this, i might convert the scripts to shell to make them more
universal. in the mean time, enjoy the cows.

References
----------

- [NAT64](https://en.wikipedia.org/wiki/NAT64)

- [DNS64](https://en.wikipedia.org/wiki/IPv6_transition_mechanism#DNS64)

- [Tayga](http://www.litech.org/tayga/)

- [Vagrant](https://www.vagrantup.com/)

- [Ansible](https://www.ansible.com/)

- [Connecting an IPv6 only site to an IPv4 only world](http://www.routereflector.com/2014/02/connecting-an-ipv6-only-site-to-an-ipv4-only-world/) by Andrea Dainese
